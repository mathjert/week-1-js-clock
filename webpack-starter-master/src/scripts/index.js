import '../styles/index.scss';

var moment = require('moment');

function timeFun() {
    document.getElementById("clockLocation").innerHTML = `<p>${moment().format('DD:MM:YYYY')}`;
    document.getElementById("clockLocation").innerHTML += ` ${moment().format('HH:mm:ss')}<\p>`;
}


setInterval(timeFun, 1000);